use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Error, Debug, Serialize, Deserialize)]
pub enum LibrarianError {
    #[error("SQLite error: {0}")]
    SqlError(String),
    #[error("Invalid admin token")]
    InvalidAdminToken,
    #[error("error adding book category: already exists")]
    CategoryAlreadyExist,
    #[error("bad book category: category name {0:?} doesn't exist")]
    BadCategory(Vec<String>),
    #[error("book with ID {0} doesn't exist")]
    BadBookID(i64),
    #[error("borrow time too short ({0} weeks)")]
    BorrowTimeTooShort(i64),
    #[error("book ID {0} is already borrowed by {1} at {2}")]
    AlreadyBorrowed(i64, String, String),
    #[error("returning or renewing a not-borrowed book (BookID {0})")]
    NotBorrowed(i64),
    #[error("renewing a book that are not borrowed by such member ({0} vs {1})")]
    WrongBorrower(String, String),
    #[error("new book doesn't have a title")]
    InvalidNewBook,
}

impl From<rusqlite::Error> for LibrarianError {
    fn from(e: rusqlite::Error) -> LibrarianError {
        LibrarianError::SqlError(e.to_string())
    }
}
