use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

#[derive(Debug, Serialize, Deserialize)]
pub struct Book {
    pub id: i64,
    pub title: String,
    pub isbn: Option<String>,
    pub subtitle: Option<String>,
    pub authors: Option<String>,
    pub category: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BookDetailed {
    pub id: i64,
    pub title: String,
    pub subtitle: Option<String>,
    pub authors: Option<String>,
    pub category: Vec<String>,
    pub isbn: Option<String>,
    pub lccn: Option<String>,
    pub edition: Option<String>,
    pub publisher: Option<String>,
    pub publish_year: Option<i64>,
    pub publish_month: Option<String>,
    pub publish_location: Option<String>,
    pub pages: Option<i64>,
    pub weight: Option<String>,
    #[serde(with = "time::serde::rfc3339")]
    pub last_updated: OffsetDateTime,
    pub deleted: bool,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct ModifyBook {
    pub title: Option<String>,
    pub subtitle: Option<String>,
    pub authors: Option<String>,
    pub category: Option<Vec<String>>,
    pub isbn: Option<String>,
    pub lccn: Option<String>,
    pub edition: Option<String>,
    pub publisher: Option<String>,
    pub publish_year: Option<i64>,
    pub publish_month: Option<String>,
    pub publish_location: Option<String>,
    pub pages: Option<i64>,
    pub weight: Option<String>,
}
