use super::{Book, BookDetailed, ModifyBook};

use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

/// Generic type for librarian response
#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum LibrarianResponse {
    Ok { success: bool, result: LibrarinOperationResponse },
    Err { success: bool, reason: String },
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum LibrarinOperationResponse {
    Borrow {
        #[serde(with = "time::serde::rfc3339")]
        due: OffsetDateTime,
    },
    Renew {
        #[serde(with = "time::serde::rfc3339")]
        new_due: OffsetDateTime,
    },
    GetAllBooks(Vec<Book>),
    GetBookDetail(BookDetailed),
    AddBook {
        book_id: i64,
    },
    GetAllCategory(Vec<String>),
    // Operations that don't require return data
    Null,
}

/// Used for /book/:id POST method
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "operation", content = "details")]
pub enum BookOperation {
    Modify(ModifyBook),
    Borrow { watid: String, borrow_weeks: i64 },
    Renew { watid: String, renew_weeks: i64 },
    Return,
}
