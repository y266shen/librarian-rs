use anyhow::{bail, Result};
use argh::FromArgs;
use std::{path::PathBuf, process::exit, sync::Arc};
use tokio::sync::Mutex;

mod db;
mod web;

#[derive(FromArgs, Debug)]
/// Librarian API server for University of Waterloo, Computer Science Club
pub struct Args {
    /// address to bind to (Default: localhost)
    #[argh(option, default = "String::from(\"localhost\")")]
    pub bind: String,
    /// port to listen on (Default: 3000)
    #[argh(option, default = "3000")]
    pub port: u32,
    /// admin token used by /api/admin (Default: none, disable admin api)
    #[argh(option)]
    pub admin_token: Option<String>,
    /// SQLite 3 database path
    #[argh(positional)]
    pub db_path: PathBuf,
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    // initialize tracing
    tracing_subscriber::fmt::init();

    if let Err(e) = try_main().await {
        eprintln!("{e}");
        exit(1);
    }
}

async fn try_main() -> Result<()> {
    // initialize args
    let args: Args = argh::from_env();
    if !args.db_path.is_file() {
        bail!("database file doesn't exist at {}!", args.db_path.display());
    }
    let db = db::CscLibraryDb::new(&args.db_path)?;

    // Start web API server
    let db_mutex = Arc::new(Mutex::new(db));
    let listen_addr = format!("{}:{}", args.bind, args.port);
    log::info!("Starting server on {listen_addr}");
    web::start(db_mutex, &listen_addr, args.admin_token).await?;
    Ok(())
}
