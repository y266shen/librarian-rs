use rusqlite::types::Value;

use librarin_api::ModifyBook;
macro_rules! option_to_sql {
    ($self:ident, $($x:ident),*) => {
        {
            let mut keys = String::new();
            let mut sql = String::new();
            let mut values = Vec::new();
            $(
                if let Some(value) = $self.$x.as_ref() {
                    keys.push_str(&format!("{},", stringify!($x)));
                    let sql_subcommand = format!("?,");
                    sql.push_str(&sql_subcommand);
                    values.push(Value::from(value.to_owned()));
                }
            )*
            let keys = keys.trim_end_matches(",").to_string();
            let sql = sql.trim_end_matches(",").to_string();
            (keys, sql, values)
        }
    };
}

pub fn modify_book_to_sql(book: &ModifyBook) -> (String, String, Vec<Value>) {
    let (keys, sql, values) = option_to_sql![
        book,
        title,
        subtitle,
        authors,
        isbn,
        lccn,
        edition,
        publisher,
        publish_year,
        publish_month,
        publish_location,
        pages,
        weight
    ];
    (keys, sql, values)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_modify_book_struct_to_sql() {
        let mut book = ModifyBook::default();
        assert_eq!(modify_book_to_sql(&book), ("".to_string(), "".to_string(), Vec::new()));
        book.title = Some("new title".to_owned());
        book.authors = Some("new authors".to_owned());
        book.pages = Some(42);
        assert_eq!(
            modify_book_to_sql(&book),
            (
                "title,authors,pages".to_owned(),
                "?,?,?".to_owned(),
                vec![
                    Value::from("new title".to_owned()),
                    Value::from("new authors".to_owned()),
                    Value::from(42)
                ]
            )
        );
    }
}
