mod sql;

use anyhow::Result;
use librarin_api::{Book, BookDetailed, LibrarianError, ModifyBook};
use rusqlite::{types::Value, Connection, OpenFlags, OptionalExtension};
use std::{collections::HashMap, path::Path};
use time::OffsetDateTime;
use tracing::{debug, warn};

pub struct CscLibraryDb {
    conn: Connection,
    category_cache: HashMap<i64, String>,
}

impl CscLibraryDb {
    pub fn new(path: &Path) -> Result<Self, LibrarianError> {
        let conn = Connection::open_with_flags(path, OpenFlags::SQLITE_OPEN_READ_WRITE)?;
        let category_cache = get_all_category(&conn)?;
        Ok(CscLibraryDb { conn, category_cache })
    }

    pub fn current_borrow(
        &self,
        id: i64,
    ) -> Result<Option<(String, OffsetDateTime)>, LibrarianError> {
        let mut stmt = self.conn.prepare(
            "
SELECT watid,time_borrow
FROM borrow
WHERE book_id=? and time_return IS NULL",
        )?;
        let record_iter = stmt.query_map([id], |row| Ok((row.get(0)?, row.get(1)?)))?;
        let record_tuples: rusqlite::Result<Vec<(String, OffsetDateTime)>> = record_iter.collect();
        let mut records = record_tuples?;
        if records.is_empty() {
            Ok(None)
        } else if records.len() > 1 {
            // The database is not sound. Report this issue
            warn!(
                "Book ID {id} is borrowed multiple({}) times! Using the latest record.",
                records.len()
            );
            Ok(Some(records.remove(0)))
        } else {
            Ok(Some(records.remove(0)))
        }
    }

    // week: number of weeks to borrow
    pub fn borrow_book(
        &self,
        id: i64,
        watid: &str,
        week: i64,
    ) -> Result<OffsetDateTime, LibrarianError> {
        if week < 1 {
            return Err(LibrarianError::BorrowTimeTooShort(week));
        } else if let Some((watid, time)) = self.current_borrow(id)? {
            // Check if there's a current borrow
            return Err(LibrarianError::AlreadyBorrowed(id, watid, time.to_string()));
        }

        // Insert a borrow record
        let sql = format!(
            "
INSERT INTO borrow (book_id,watid,time_borrow,time_due)
VALUES (?1, ?2, ?3, datetime(?3, '+{} days'))",
            week * 7
        );
        self.conn.execute(&sql, (id, watid, OffsetDateTime::now_utc()))?;
        let row = self.conn.last_insert_rowid();
        // Get due timestamp
        let mut stmt = self.conn.prepare("SELECT time_due FROM borrow WHERE rowid=?")?;
        let due: OffsetDateTime = stmt.query_row([row], |row| row.get(0))?;

        Ok(due)
    }

    pub fn renew_book(
        &self,
        id: i64,
        requester_watid: &str,
        week: i64,
    ) -> Result<OffsetDateTime, LibrarianError> {
        if week < 1 {
            return Err(LibrarianError::BorrowTimeTooShort(week));
        }

        if let Some((watid, time_borrow)) = self.current_borrow(id)? {
            if requester_watid != watid {
                return Err(LibrarianError::WrongBorrower(requester_watid.to_owned(), watid));
            }
            let sql = format!(
                "
UPDATE borrow
SET time_due=datetime(time_due, '+{} days')
WHERE book_id=?1 and watid=?2 and time_borrow=?3",
                week * 7
            );
            self.conn.execute(&sql, (id, &watid, time_borrow))?;
            debug!("{time_borrow}");
            // Get the new due time
            let mut stmt = self.conn.prepare(
                "SELECT time_due FROM borrow WHERE book_id=?1 and watid=?2 and time_borrow=?3",
            )?;
            let due: OffsetDateTime = stmt.query_row((id, watid, time_borrow), |row| row.get(0))?;
            Ok(due)
        } else {
            Err(LibrarianError::NotBorrowed(id))
        }
    }

    pub fn return_book(&self, id: i64) -> Result<(), LibrarianError> {
        // Check if there's a current borrow
        if let Some((watid, time_borrow)) = self.current_borrow(id)? {
            self.conn.execute(
                "
UPDATE borrow
SET time_return=CURRENT_TIMESTAMP
WHERE book_id=?1 and watid=?2 and time_borrow=?3",
                (id, watid, time_borrow),
            )?;
            Ok(())
        } else {
            Err(LibrarianError::NotBorrowed(id))
        }
    }

    fn update_cagegory_cache(&mut self) -> Result<(), LibrarianError> {
        self.category_cache = get_all_category(&self.conn)?;
        Ok(())
    }

    pub fn category_exist(&self, name: &str) -> bool {
        self.category_cache.values().any(|v| v.as_str() == name)
    }

    pub fn get_all_categories(&self) -> Vec<String> {
        self.category_cache.values().map(|x| x.clone()).collect()
    }

    pub fn new_category(&mut self, cat_name: &str) -> Result<(), LibrarianError> {
        // Check if we have this category already
        if self.category_exist(cat_name) {
            return Err(LibrarianError::CategoryAlreadyExist);
        }

        self.conn.execute("INSERT INTO categories (category) VALUES (?)", [cat_name])?;
        // Update cache
        self.update_cagegory_cache()?;
        Ok(())
    }

    pub fn del_category(&mut self, cat_name: &str) -> Result<(), LibrarianError> {
        // Check if we have this category already
        if self.category_exist(cat_name) {
            return Err(LibrarianError::CategoryAlreadyExist);
        }

        self.conn.execute("DELETE FROM categories WHERE category=?", [cat_name])?;
        // Update cache
        self.update_cagegory_cache()?;
        Ok(())
    }

    fn category_name_to_id(&self, mut cat_names: Vec<String>) -> (Vec<i64>, Vec<String>) {
        let mut cat_ids: Vec<i64> = Vec::with_capacity(cat_names.len());
        for (cat_id, cat_name) in &self.category_cache {
            if let Some(i) = cat_names.iter().position(|x| x == cat_name) {
                cat_ids.push(*cat_id);
                cat_names.remove(i);
            }
        }
        (cat_ids, cat_names)
    }

    pub fn get_book_category(&self, id: i64) -> rusqlite::Result<Vec<String>, rusqlite::Error> {
        let mut stmt = self.conn.prepare("SELECT cat_id FROM book_categories WHERE id = ?")?;
        let cat_ids: Vec<i64> =
            stmt.query_map([id], |row| row.get(0))?.collect::<rusqlite::Result<Vec<i64>>>()?;
        let cat_names: Vec<String> = cat_ids
            .into_iter()
            .map(|cat_id| self.category_cache.get(&cat_id).unwrap().clone())
            .collect();
        Ok(cat_names)
    }

    pub fn set_book_category(&self, id: i64, cat_names: Vec<String>) -> Result<(), LibrarianError> {
        // First delete all current ones
        self.conn.execute("DELETE FROM book_categories WHERE id=?", [id])?;
        // Then add all of them back
        let (cat_ids, unknown_cat_names) = self.category_name_to_id(cat_names);
        if !unknown_cat_names.is_empty() {
            return Err(LibrarianError::BadCategory(unknown_cat_names));
        }

        for cat_id in cat_ids {
            self.conn
                .execute("INSERT INTO book_categories (id, cat_id) VALUES (?, ?)", [id, cat_id])?;
        }

        Ok(())
    }

    pub fn get_all_books(&self) -> Result<Vec<Book>, LibrarianError> {
        let mut stmt = self.conn.prepare(
            "
SELECT id,isbn,title,subtitle,authors
FROM books",
        )?;
        let book_iter = stmt.query_map([], |row| {
            let id: i64 = row.get(0)?;
            let cat_names = self.get_book_category(id)?;
            Ok(Book {
                id,
                isbn: row.get(1)?,
                title: row.get(2)?,
                subtitle: row.get(3)?,
                authors: row.get(4)?,
                category: cat_names,
            })
        })?;

        let books: rusqlite::Result<Vec<Book>> = book_iter.collect();
        Ok(books?)
    }

    pub fn get_book_detailed(&self, id: i64) -> Result<BookDetailed, LibrarianError> {
        let mut stmt = self.conn.prepare(
            "
SELECT id,title,subtitle,authors,isbn,lccn,edition,
       publisher,publish_year,publish_month,publish_location,
       pages,weight,last_updated,deleted
FROM books
WHERE id=?1",
        )?;
        let book_detailed = stmt
            .query_row([id], |row| {
                let id: i64 = row.get(0)?;
                let cat_name = self.get_book_category(id)?;
                Ok(BookDetailed {
                    id,
                    title: row.get(1)?,
                    subtitle: row.get(2)?,
                    authors: row.get(3)?,
                    category: cat_name,
                    isbn: row.get(4)?,
                    lccn: row.get(5)?,
                    edition: row.get(6)?,
                    publisher: row.get(7)?,
                    publish_year: row.get(8)?,
                    publish_month: row.get(9)?,
                    publish_location: row.get(10)?,
                    pages: row.get(11)?,
                    weight: row.get(12)?,
                    last_updated: row.get(13)?,
                    deleted: row.get(14)?,
                })
            })
            .optional()?;

        if let Some(book_detailed) = book_detailed {
            Ok(book_detailed)
        } else {
            Err(LibrarianError::BadBookID(id))
        }
    }

    pub fn add_book(&self, book: &ModifyBook) -> Result<i64, LibrarianError> {
        // Do some checks beforehand
        if book.title.is_none() {
            // Bad Request
            return Err(LibrarianError::InvalidNewBook);
        }

        if let Some(category) = book.category.clone() {
            let (_, unknown_category_names) = self.category_name_to_id(category);
            if !unknown_category_names.is_empty() {
                return Err(LibrarianError::BadCategory(unknown_category_names));
            }
        }

        let (keys, sql, values) = sql::modify_book_to_sql(book);
        let sql_query = format!("INSERT INTO books ({keys}) VALUES ({sql})");
        debug!("{sql_query}");
        self.conn.execute(&sql_query, rusqlite::params_from_iter(values.iter()))?;
        let book_id = self.conn.last_insert_rowid();

        // Set categories
        if let Some(category) = book.category.clone() {
            self.set_book_category(book_id, category)?;
        }

        Ok(book_id)
    }

    pub fn update_book(&self, id: i64, book: &ModifyBook) -> Result<(), LibrarianError> {
        if let Some(category) = book.category.clone() {
            let (_, unknown_category_names) = self.category_name_to_id(category.clone());
            if !unknown_category_names.is_empty() {
                return Err(LibrarianError::BadCategory(unknown_category_names));
            }
            self.set_book_category(id, category)?;
        }

        let (_, sql, values) = sql::modify_book_to_sql(book);
        let sql_command = if sql.is_empty() {
            "UPDATE books SET last_updated=CURRENT_TIMESTAMP WHERE id=?".to_owned()
        } else {
            format!("UPDATE books SET {sql},last_updated=CURRENT_TIMESTAMP WHERE id=?",)
        };
        self.conn.execute(
            &sql_command,
            rusqlite::params_from_iter(values.iter().chain([Value::from(id)].iter())),
        )?;
        Ok(())
    }

    pub fn del_book(&self, id: i64) -> Result<(), LibrarianError> {
        self.conn.execute("DELETE FROM books WHERE id=?", [id])?;
        Ok(())
    }
}

fn get_all_category(db: &Connection) -> Result<HashMap<i64, String>, LibrarianError> {
    let mut stmt = db.prepare("SELECT cat_id,category FROM categories")?;
    let cat_iter = stmt.query_map([], |row| Ok((row.get(0)?, row.get(1)?)))?;
    let categorie_tuples: rusqlite::Result<Vec<(i64, String)>> = cat_iter.collect();
    Ok(categorie_tuples?.into_iter().collect())
}
