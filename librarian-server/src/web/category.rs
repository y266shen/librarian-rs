use super::{ApiResult, ApiState};
use librarin_api::*;

use axum::extract::{Path, State};
use tracing::info;

pub async fn all(State(state): State<ApiState>) -> ApiResult {
    let db = state.db.lock().await;
    let res = LibrarianResponse::Ok {
        success: true,
        result: LibrarinOperationResponse::GetAllCategory(db.get_all_categories()),
    };
    Ok(res.into())
}

pub async fn new(State(state): State<ApiState>, Path(cat_name): Path<String>) -> ApiResult {
    let mut db = state.db.lock().await;
    db.new_category(&cat_name)?;
    info!("new category {cat_name} added");
    let res = LibrarianResponse::Ok { success: true, result: LibrarinOperationResponse::Null };
    Ok(res.into())
}

pub async fn del(State(state): State<ApiState>, Path(cat_name): Path<String>) -> ApiResult {
    let mut db = state.db.lock().await;
    db.del_category(&cat_name)?;
    let res = LibrarianResponse::Ok { success: true, result: LibrarinOperationResponse::Null };
    Ok(res.into())
}
