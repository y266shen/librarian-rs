use super::{ApiResult, ApiState};
use librarin_api::*;

use axum::extract::{Query, State};
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Admin {
    token: String,
}

pub async fn check(State(state): State<ApiState>, admin: Query<Admin>) -> ApiResult {
    let err_invalid_token = LibrarianError::InvalidAdminToken;
    if let Some(secret) = state.admin_token {
        if admin.token != secret {
            return Err(err_invalid_token.into());
        }
    } else {
        return Err(err_invalid_token.into());
    }

    let db = state.db.lock().await;
    // Read all book details once
    let books = db.get_all_books()?;
    for book in books {
        if let Err(e) = db.get_book_detailed(book.id) {
            let res = LibrarianError::SqlError(format!("bad book {}: {e}", book.id));
            return Err(res.into());
        }
        for category in book.category {
            if !db.category_exist(&category) {
                let res = LibrarianError::SqlError(format!("bad category {} for book {}", category, book.id));
                return Err(res.into());
            }
        }
    }

    let res = LibrarianResponse::Ok { success: true, result: LibrarinOperationResponse::Null };
    Ok(res.into())
}
