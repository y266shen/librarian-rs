use super::{ApiResult, ApiState};
use librarin_api::*;

use axum::{
    extract::{Path, State},
    Json,
};
use tracing::info;

pub async fn all(State(state): State<ApiState>) -> ApiResult {
    let db = state.db.lock().await;
    let res = LibrarianResponse::Ok {
        success: true,
        result: LibrarinOperationResponse::GetAllBooks(db.get_all_books()?),
    };
    Ok(res.into())
}

pub async fn detail(State(state): State<ApiState>, Path(book_id): Path<i64>) -> ApiResult {
    let db = state.db.lock().await;
    let res = LibrarianResponse::Ok {
        success: true,
        result: LibrarinOperationResponse::GetBookDetail(db.get_book_detailed(book_id)?),
    };
    Ok(res.into())
}

pub async fn add(State(state): State<ApiState>, Json(payload): Json<ModifyBook>) -> ApiResult {
    let db = state.db.lock().await;
    let id = db.add_book(&payload)?;
    info!("added book {} with id {id}", &payload.title.unwrap());
    let res = LibrarianResponse::Ok { success: true, result: LibrarinOperationResponse::Null };
    Ok(res.into())
}

pub async fn update(
    State(state): State<ApiState>,
    Path(book_id): Path<i64>,
    Json(payload): Json<BookOperation>,
) -> ApiResult {
    let db = state.db.lock().await;
    let result = match payload {
        BookOperation::Modify(b) => {
            db.update_book(book_id, &b)?;
            info!("updated book ID {book_id}");
            LibrarinOperationResponse::Null
        }
        BookOperation::Borrow { watid, borrow_weeks } => {
            let res = db.borrow_book(book_id, &watid, borrow_weeks)?;
            info!("borrowed book ID {book_id} by {watid} for {borrow_weeks} weeks");
            LibrarinOperationResponse::Borrow { due: res }
        }
        BookOperation::Renew { watid, renew_weeks } => {
            let res = db.renew_book(book_id, &watid, renew_weeks)?;
            info!("renewed book ID {book_id} by {watid} for {renew_weeks} weeks");
            LibrarinOperationResponse::Renew { new_due: res }
        }
        BookOperation::Return => {
            db.return_book(book_id)?;
            info!("returned book ID {book_id}");
            LibrarinOperationResponse::Null
        }
    };
    let response = LibrarianResponse::Ok { success: true, result };
    Ok(response.into())
}

pub async fn del(State(state): State<ApiState>, Path(book_id): Path<i64>) -> ApiResult {
    let db = state.db.lock().await;
    // TODO: Check if have borrow records or category data
    db.del_book(book_id)?;
    let res = LibrarianResponse::Ok { success: true, result: LibrarinOperationResponse::Null };
    Ok(res.into())
}
