use crate::db::CscLibraryDb;

mod admin;
mod book;
mod category;

use axum::{
    http::{header, StatusCode},
    response::{IntoResponse, Response},
    routing::{get, post},
    Json, Router,
};
use librarin_api::{LibrarianError, LibrarianResponse};
use std::sync::Arc;
use tokio::sync::Mutex;
use tower_http::{trace::{self, TraceLayer}, cors::CorsLayer};
use tracing::Level;

lazy_static::lazy_static! {
    static ref SERVER_NAME: String = format!("librarian-rs {}", env!("CARGO_PKG_VERSION"));
}
#[derive(Clone)]
pub struct ApiState {
    db: Arc<Mutex<CscLibraryDb>>,
    admin_token: Option<String>,
}

pub async fn start(
    db: Arc<Mutex<CscLibraryDb>>,
    listen_addr: &str,
    admin_token: Option<String>,
) -> anyhow::Result<()> {
    let app = Router::new()
        .route("/", get(root))
        .route("/api", get(api_root))
        .route("/api/books", get(book::all).post(book::add))
        .route("/api/books/{id}", get(book::detail).put(book::update).delete(book::del))
        .route("/api/categories", get(category::all))
        .route("/api/categories/{cat_name}", post(category::new).delete(category::del))
        .route("/api/admin/check", post(admin::check))
        .with_state(ApiState { db, admin_token })
        .layer(CorsLayer::permissive())
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(trace::DefaultMakeSpan::new().level(Level::INFO))
                .on_response(trace::DefaultOnResponse::new().level(Level::INFO)),
        );

    let listener = tokio::net::TcpListener::bind(listen_addr).await?;
    axum::serve(listener, app).await?;
    Ok(())
}

async fn root() -> &'static str {
    "Use /api for Librarian-rs api calls."
}

async fn api_root() -> &'static str {
    "Librarian-rs Ready."
}

pub type ApiResult = core::result::Result<ApiResponse, ApiError>;

#[derive(derive_more::From)]
pub struct ApiResponse(LibrarianResponse);
impl IntoResponse for ApiResponse {
    fn into_response(self) -> Response {
        (StatusCode::OK, [(header::SERVER, SERVER_NAME.as_str())], Json::from(self.0))
            .into_response()
    }
}

#[derive(derive_more::From)]
pub struct ApiError(LibrarianError);
impl IntoResponse for ApiError {
    fn into_response(self) -> Response {
        let code = match self.0 {
            LibrarianError::SqlError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            _ => StatusCode::BAD_REQUEST,
        };
        let res = LibrarianResponse::Err { success: false, reason: self.0.to_string() };
        (code, [(header::SERVER, SERVER_NAME.as_str())], Json::from(res)).into_response()
    }
}
