mod actions;
mod config;
use config::Args;
mod cli;
mod pager;

use anyhow::Result;
use clap::Parser;
use std::sync::atomic::AtomicI32;

// Initialize writer
lazy_static::lazy_static! {
    static ref WRITER: cli::Writer = cli::Writer::new();
}
static SUBPROCESS: AtomicI32 = AtomicI32::new(-1);

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args: Args = Args::parse();

    if let Err(err) = try_main(&args).await {
        // Create a new line first, for visual distinction
        WRITER.writeln("", "").ok();
        error!("{}", err.to_string());
        err.chain().skip(1).for_each(|cause| {
            due_to!("{}", cause);
        });
    }
}

async fn try_main(args: &config::Args) -> Result<()> {
    actions::fulfill_command(args).await?;
    Ok(())
}
