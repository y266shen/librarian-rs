use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
/// librarian-rs CLI client
pub struct Args {
    /// enable verbose logging
    #[arg(short)]
    pub verbose: bool,

    /// librarian-rs server address
    #[arg(short, long)]
    pub address: String,

    #[command(subcommand)]
    pub command: Command,
}

#[derive(Subcommand, Debug)]
pub enum Command {
    /// add a new book
    Add,
    /// delete a book from the catalogue
    Del,
    /// list all books in the catalogue
    List,
    /// get book detail
    Get,
    /// borrow a book
    Borrow,
    /// renew a borrowed book
    Renew,
    /// return a book
    Return,
    /// list, add or remove categories
    #[command(subcommand)]
    Category,
}

#[derive(Subcommand, Debug)]
pub enum Category {
    /// List all categories
    List,
    /// Add a new category
    Add,
    ///
    Del,
}
