mod add;
mod del;
mod list;

use crate::config::*;

use crate::config;
use anyhow::Result;

pub async fn fulfill_command(args: &config::Args) -> Result<()> {
    match &args.command {
        config::Command::List => list::get_all(&args.address).await?,
        config::Command::Add => add::add(&args.address).await?,
        config::Command::Del => del::del(&args.address).await?,
        _ => unimplemented!(),
    };
    Ok(())
}
