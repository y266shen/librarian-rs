use anyhow::{bail, Result};
use librarin_api::{LibrarianResponse, LibrarinOperationResponse};
use reqwest::Client;
use tabled::{settings::Style, Table, Tabled};

#[derive(Tabled)]
struct BookRow {
    pub id: i64,
    pub title: String,
    pub subtitle: String,
    pub authors: String,
    pub category: String,
}

pub async fn get_all(addr: &str) -> Result<()> {
    let c = Client::new();

    // First read books from the server
    let books_path = format!("{addr}/books");
    let result: LibrarianResponse = c.get(&books_path).send().await?.json().await?;

    let books = match result {
        LibrarianResponse::Ok { success: _, result } => match result {
            LibrarinOperationResponse::GetAllBooks(books) => books,
            _ => unreachable!(),
        },
        LibrarianResponse::Err { success: _, reason } => bail!(reason),
    };

    let rows: Vec<BookRow> = books
        .into_iter()
        .map(|book| BookRow {
            id: book.id,
            title: book.title,
            subtitle: book.subtitle.unwrap_or_default(),
            authors: book.authors.unwrap_or_default(),
            category: book.category.join(",").to_owned(),
        })
        .collect();
    let table = Table::new(rows).with(Style::psql()).to_string();

    // Start pager
    let mut pager = crate::pager::Pager::new(false)?;
    {
        let mut writer = pager.get_writer()?;
        writer.write_all(table.as_bytes())?;
    }
    pager.wait_for_exit()?;

    Ok(())
}
