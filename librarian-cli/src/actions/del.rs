use librarin_api::{LibrarianResponse, LibrarinOperationResponse, ModifyBook};

use anyhow::{bail, Result};
use inquire::CustomType;
use reqwest::{Client, StatusCode};

pub async fn del(addr: &str) -> Result<()> {
    let c = Client::new();

    let book_id: i64 = CustomType::new("Book ID:").prompt()?;
    let book_path = format!("{addr}/books/{book_id}");
    let response: LibrarianResponse = c.delete(&book_path).send().await?.json().await?;

    match response {
        LibrarianResponse::Ok { success: _, result: _ } => Ok(()),
        LibrarianResponse::Err { success: _, reason } => bail!(reason),
    }
}
