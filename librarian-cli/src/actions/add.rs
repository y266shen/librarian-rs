use librarin_api::{LibrarianResponse, LibrarinOperationResponse, ModifyBook};

use anyhow::{bail, Result};
use inquire::{
    error::{CustomUserError, InquireResult},
    required, validator, CustomType, MultiSelect, Select, Text,
};
use reqwest::{Client, StatusCode};

pub async fn add(addr: &str) -> Result<()> {
    let c = Client::new();

    // First get all categories from the server
    let all_categories_path = format!("{addr}/categories");
    let all_categories_response = c.get(&all_categories_path).send().await?.json().await?;
    let all_categories = match all_categories_response {
        LibrarianResponse::Ok { success: _, result } => match result {
            LibrarinOperationResponse::GetAllCategory(categories) => categories,
            _ => unreachable!(),
        },
        LibrarianResponse::Err { success: _, reason } => bail!(reason),
    };

    // Then ask users
    let validator = validator::MinLengthValidator::new(1);

    let book = ModifyBook {
        title: Some(Text::new("Title:").with_validator(validator.clone()).prompt()?),
        subtitle: Text::new("Subtitle:").with_validator(validator.clone()).prompt_skippable()?,
        authors: Text::new("Authors:").with_validator(validator.clone()).prompt_skippable()?,
        category: MultiSelect::new("Book categories:", all_categories)
            .with_validator(validator.clone())
            .prompt_skippable()?,
        //
        isbn: Text::new("ISBN:").with_validator(validator.clone()).prompt_skippable()?,
        lccn: Text::new("LCCN:").with_validator(validator.clone()).prompt_skippable()?,
        edition: Text::new("Edition:").with_validator(validator.clone()).prompt_skippable()?,
        publisher: Text::new("Publisher:").with_validator(validator.clone()).prompt_skippable()?,
        publish_year: CustomType::new("Publish year:").prompt_skippable()?,
        publish_month: Text::new("Publish month:")
            .with_validator(validator.clone())
            .prompt_skippable()?,
        publish_location: Text::new("Publish location:")
            .with_validator(validator.clone())
            .prompt_skippable()?,
        pages: CustomType::new("Pages:").prompt_skippable()?,
        weight: Text::new("Weight:").with_validator(validator).prompt_skippable()?,
    };

    let books_path = format!("{addr}/books");
    let response: LibrarianResponse = c.post(&books_path).json(&book).send().await?.json().await?;
    match response {
        LibrarianResponse::Ok { success: _, result: _ } => Ok(()),
        LibrarianResponse::Err { success: _, reason } => bail!(reason),
    }
}
